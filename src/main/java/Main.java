
import com.towersoft.caller.NativeCaller;
import com.towersoft.caller.Result;
import java.io.File;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Asus
 */
public class Main {

    private static String OS = null;

    public static String getOsName() {
        if (OS == null) {
            OS = System.getProperty("os.name");
        }
        return OS;
    }

    public static boolean isWindows() {
        return getOsName().startsWith("Windows");
    }

    public static void main(String[] args) {

        String name;
        if (isWindows()) {

            if (System.getProperty("sun.arch.data.model").equals("64")) {
                name = "NativeCaller_x86_64.dll";
            } else {
                name = "NativeCaller_x86.dll";
            }

        } else if (System.getProperty("sun.arch.data.model").equals("64")) {
            name = "NativeCaller_x86_64.so";
        } else {
            name = "NativeCaller_x86.so";
        }

        File f = new File(name);
        System.load(f.getAbsolutePath());

        NativeCaller nativePathEvaluator = new NativeCaller();
        Result result = new Result();
        //nativePathEvaluator.doEvaluate("%RUBY_EXE% d:\\dcim\\hello.rb", result);
        nativePathEvaluator.doEvaluate("%CURL% http://localhost:8000",result);
        //nativePathEvaluator.doEvaluate("ping 127.0.0.1",result);
        System.out.println(""+result.text);
        System.out.println(""+result.status);
        
    }

}
 