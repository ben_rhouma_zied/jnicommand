package com.towersoft.caller;


/**
 *
 * @author Asus
 */
public class NativeCaller {

    private native String evaluate(String command, Result res);

    public String doEvaluate(String cmd,Result res) {
        return evaluate(cmd,res);
    }
}
