javah com.towersoft.caller.NativeCaller
copy com_towersoft_caller_NativeCaller.h csrc\.
rm com_towersoft_caller_NativeCaller.h
cd csrc
rm NativeCaller_x86_64.o NativeCaller_x86_64.dll
rm NativeCaller_x86.o NativeCaller_x86.dll

x86_64-w64-mingw32-g++ -c -I"%JDK_HOME%\include" -I"%JDK_HOME%\include\win32" -m64 NativeCaller.cpp -o NativeCaller_x86_64.o
x86_64-w64-mingw32-g++ -Wl,--add-stdcall-alias -shared -m64 -o NativeCaller_x86_64.dll NativeCaller_x86_64.o

g++ -c -I"%JDK_HOME%\include" -I"%JDK_HOME%\include\win32" NativeCaller.cpp -o NativeCaller_x86.o
g++ -Wl,--add-stdcall-alias -shared -o NativeCaller_x86.dll NativeCaller_x86.o

copy NativeCaller_x86_64.dll ..\..\..\..
copy NativeCaller_x86.dll ..\..\..\..
rm NativeCaller_x86_64.o NativeCaller_x86_64.dll
rm NativeCaller_x86.o NativeCaller_x86.dll

cd ..

