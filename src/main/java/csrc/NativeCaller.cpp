#include <jni.h>
#include <cstdlib>
#include<strings.h>
#include <iostream>
#include <stdio.h>

#include "com_towersoft_caller_NativeCaller.h"
using namespace std;

struct result {
    int status;
    string text;
};

result GetStdoutFromCommand(string cmd) {
    string data;
    FILE * stream;
    const int max_buffer = 256;
    char buffer[max_buffer];
    cmd.append(" 2>&1");

    stream = popen(cmd.c_str(), "r");
    int status;
    result res;
    if (stream) {
        while (!feof(stream))
            if (fgets(buffer, max_buffer, stream) != NULL) data.append(buffer);
        res.status = pclose(stream);
        res.text = data;
    }
    return res;
}

JNIEXPORT jstring JNICALL Java_com_towersoft_caller_NativeCaller_evaluate
(JNIEnv *env, jobject obj, jstring string, jobject resObj) {

    const char *command = env->GetStringUTFChars(string, 0);
    result resultStruct = GetStdoutFromCommand(command);
    jstring commndJ = env->NewStringUTF(command);
    
    jclass resultClass = env->GetObjectClass(resObj);
    
    jfieldID textField = env->GetFieldID(resultClass, "text", "Ljava/lang/String;");
    jfieldID statusField = env->GetFieldID(resultClass, "status", "I");
    
    env->SetObjectField(resObj,textField, env->NewStringUTF(resultStruct.text.c_str()));
    env->SetIntField(resObj,statusField,(jint) resultStruct.status);
    
    return commndJ;


}



